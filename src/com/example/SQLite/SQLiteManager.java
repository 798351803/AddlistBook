package com.example.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SQLiteManager {
	
	private SQLiteOpenHelper openHelper;
	private SQLiteDatabase db;
	
	public  SQLiteManager(Context context){
		openHelper=new SQLiteOpenHelper(context);
		db=openHelper.getWritableDatabase();
	}
	
	public void Insert(SQLiteText text){
		ContentValues values=new ContentValues();
		values.put("name", text.getName());
		values.put("phone", text.getPhone());
		db.insert("people", null, values);
		db.close();
	}
	
	public void deleteAll(){
		db.delete("people", null, null);
	}
	
	public void delete(SQLiteText text){
		db.delete("people", "name=?", new String[] {text.getName()});
	}

	public Cursor selectAll(){
		Cursor c= db.rawQuery("SELECT * FROM people", null);
		return c;
	}
	
	public Cursor select(SQLiteText text){
		Cursor c=db.rawQuery("SELECT * FROM people", new String[] {text.getName()});
		return c;
	}
	
	public void alter(SQLiteText text){
		//db.execSQL("update people set name=?,phone=? where _id=? ", new Object[] {text.getName(),text.getPhone(),text.get_id()});
		ContentValues cv=new ContentValues();
		cv.put("name", text.getName());
		cv.put("phone", text.getPhone());
		db.update("people", cv, "_id=? ", new String[] {String.valueOf(text.get_id())});
		db.close();
	}
	
}
