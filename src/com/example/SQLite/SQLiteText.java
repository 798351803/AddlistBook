package com.example.SQLite;

public class SQLiteText {

	String name;
	int phone;
	int _id;
	
	public SQLiteText(String name,int phone,int _id){
		this.name=name;
		this.phone=phone;
		this._id=_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}
	
	
}
