package com.example.addressbook;

import com.example.SQLite.SQLiteManager;
import com.example.SQLite.SQLiteText;

import android.R.integer;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class AlterMainActivity extends Activity {
	
	String name,phone,_id,Aname;
	EditText alter_name,alter_phone;
	Button alter_ok,alter_delete;
	SQLiteManager manager;
	SQLiteText text;
	int  Aphone,A_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alter_main);
		manager=new SQLiteManager(this);
		alter_name=(EditText) findViewById(R.id.alter_name);
		alter_phone=(EditText) findViewById(R.id.alter_phone);
		alter_ok=(Button) findViewById(R.id.alter_ok);
		alter_delete=(Button) findViewById(R.id.alter_delete);
		alter_ok.setOnClickListener(new AlterListener());
		alter_delete.setOnClickListener(new DeleteListener());
		
		Intent intent=getIntent();
		name=intent.getStringExtra("name");
		phone=intent.getStringExtra("phone");
		_id=intent.getStringExtra("_id");
		
		alter_name.setText(name);
		alter_phone.setText(phone);
		
	}

	class AlterListener implements android.view.View.OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Aname=alter_name.getText().toString();
			Aphone=Integer.valueOf(alter_phone.getText().toString());
			A_id=Integer.valueOf(_id);
			text=new SQLiteText(Aname, Aphone,A_id);
			manager.alter(text);
			Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(AlterMainActivity.this,MainActivity.class);
			startActivity(intent);
			AlterMainActivity.this.finish();
		}
		
	}
	
	class DeleteListener implements android.view.View.OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			text=new SQLiteText(name, Aphone,A_id);
			manager.delete(text);
			Toast.makeText(getApplicationContext(), "删除成功", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(AlterMainActivity.this,MainActivity.class);
			startActivity(intent);
			AlterMainActivity.this.finish();
		}
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode==KeyEvent.KEYCODE_BACK) {
			Intent intent=new Intent(AlterMainActivity.this, MainActivity.class);
			startActivity(intent);
			AlterMainActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}

}
