package com.example.addressbook;

import com.example.SQLite.SQLiteManager;
import com.example.SQLite.SQLiteOpenHelper;
import com.example.SQLite.SQLiteText;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddMainActivity extends Activity {

	EditText edit_name, edit_phone;
	Button ok;
	SQLiteManager Manager;
	String personid;
	SQLiteDatabase db;
	int peopleID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_main);
		SQLiteOpenHelper helper=new SQLiteOpenHelper(this);
		db=helper.getReadableDatabase();
		Manager=new SQLiteManager(this);
		edit_name = (EditText) findViewById(R.id.edit_name);
		edit_phone = (EditText) findViewById(R.id.edit_phone);
		ok = (Button) findViewById(R.id.ok);
		ok.setOnClickListener(new okListener());
	}

	class okListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			addtion();
			Intent intent = new Intent();
			intent.setClass(AddMainActivity.this, MainActivity.class);
			startActivity(intent);
			AddMainActivity.this.finish();
		}

	}

	public void addtion() {
		// TODO Auto-generated methcod stub
		String name = edit_name.getText().toString();
		int phone = Integer.parseInt(edit_phone.getText().toString());
		Cursor c=db.rawQuery("select max(_id) from people", null);
		c.moveToLast();
		int a=c.getInt(0);
		SQLiteText text = new SQLiteText(name, phone, a+1);
		Manager.Insert(text);

	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode==KeyEvent.KEYCODE_BACK) {
			Intent intent=new Intent(AddMainActivity.this, MainActivity.class);
			startActivity(intent);
			AddMainActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
