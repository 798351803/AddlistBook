package com.example.addressbook;

import java.sql.Struct;
import java.util.HashMap;

import com.example.SQLite.SQLiteManager;
import com.example.SQLite.SQLiteText;

import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	Button add;
	SQLiteManager manager;
	ListView listview ;
	int myposition,info_id;
	SQLiteDatabase db;
	String cname,cphone,c_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager=new SQLiteManager(this);
        add=(Button) findViewById(R.id.add);
        add.setOnClickListener(new addListener());
        simpleCursorAdapter();
    }

    public void simpleCursorAdapter() {
		// TODO Auto-generated method stub
		listview=(ListView) findViewById(R.id.listview);
		Cursor cursor=manager.selectAll();
		SimpleCursorAdapter adapter=new SimpleCursorAdapter(this, 
				R.layout.listview, cursor, 
				new String[] {"name","phone"}, 
				new int[] {R.id.text_name,R.id.text_phone});
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new ListListener());
    }

	class addListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent=new Intent();
			intent.setClass(MainActivity.this, AddMainActivity.class);
			startActivity(intent);
			MainActivity.this.finish();
			simpleCursorAdapter();
		}
    }
	
	
	class ListListener implements android.widget.AdapterView.OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			 ListView listView = (ListView) parent;  
			 Cursor cursor = (Cursor) listView.getItemAtPosition(position);  
			 cname=cursor.getString(cursor.getColumnIndex("name"));
			 cphone=cursor.getString(cursor.getColumnIndex("phone"));
			 c_id=cursor.getString(cursor.getColumnIndex("_id"));
			 Dialog(cname,cphone,c_id);
		}
		
	}

	public void Dialog(String name,String phone,String _id) {
		// TODO Auto-generated method stub
		AlertDialog.Builder builer=new AlertDialog.Builder(this);
		builer.setTitle("联系人");
		builer.setMessage("名字："+name+"\n电话："+phone);
		builer.setPositiveButton("拨打", new DialogLisener());
		builer.setNegativeButton("取消", new DialogLisener());
		builer.setNeutralButton("修改或删除", new DialogLisener());
		builer.create().show();
	}
	
	class DialogLisener implements DialogInterface.OnClickListener{

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				positive();
				break;

			case DialogInterface.BUTTON_NEGATIVE:
				negative();
				break;
				
			case DialogInterface.BUTTON_NEUTRAL:
				neutral();
				break;
			}
		}

		//修改按钮
		private void neutral() {
			// TODO Auto-generated method stub
			Intent intent=new Intent();
			intent.putExtra("name", cname);
			intent.putExtra("phone", cphone);
			intent.putExtra("_id", c_id);
			intent.setClass(MainActivity.this, AlterMainActivity.class);
			startActivity(intent);
			MainActivity.this.finish();
		}

		//取消按钮
		private void negative() {
			// TODO Auto-generated method stub
			
		}

		//拨打按钮
		private void positive() {
			// TODO Auto-generated method stub
			Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+cphone));
			startActivity(intent);
		}
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder=new AlertDialog.Builder(this);
		builder.setTitle("退出程序");
		builder.setMessage("是否真的退出程序");
		builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				MainActivity.this.finish();
			}
		});
		builder.setNegativeButton("取消", new DialogLisener());
		builder.create().show();
		return super.onKeyDown(keyCode, event);
	}

    
}
